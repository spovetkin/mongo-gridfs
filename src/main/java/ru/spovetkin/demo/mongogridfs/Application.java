package ru.spovetkin.demo.mongogridfs;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.spovetkin.demo.mongogridfs.mongo.MongoDBService;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@SpringBootApplication
public class Application {

    @Autowired
    MongoDBService mongoDBService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    private void postConstruct() throws IOException {
        File gitFile = new File("src/main/resources/test_git.pdf");
        // сохраняем
        InputStream fileInputStream = new FileInputStream(gitFile);
        ObjectId objectId = mongoDBService.storeFile(fileInputStream, gitFile.getName());

        // получаем
        InputStream inputStream = mongoDBService.readFile(objectId.toString());
        Files.copy(inputStream, Paths.get("new_test_git.pdf"));
    }

}
