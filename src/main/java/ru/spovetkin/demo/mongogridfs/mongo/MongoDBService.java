package ru.spovetkin.demo.mongogridfs.mongo;

import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service
public class MongoDBService {

    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    private GridFsOperations gridFsOperations;

    @Autowired
    private MongoDbFactory mongoDbFactory;

    public ObjectId storeFile(InputStream inputStream, String fileName) {
        return gridFsTemplate.store(inputStream, fileName);
    }

    public InputStream readFile(String fileId) throws IOException {
        GridFSFile file = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is(fileId)));
        if (file == null) {
            return null;
        }

        GridFsResource gridFsResource = new GridFsResource(file, getBucket().openDownloadStream(file.getObjectId()));
        return gridFsResource.getInputStream();
    }

    private GridFSBucket getBucket() {
        return GridFSBuckets.create(mongoDbFactory.getDb());
    }

}
